// $XKeyboardConfig: xkbdesc/symbols/fr,v 1.8 2005/01/16 02:18:20 svu Exp $

// based on a keyboard map from an 'xkb/symbols/fr' file
//
// $XFree86: xc/programs/xkbcomp/symbols/fr,v 1.2 2002/11/22 04:03:28 dawes Exp $

// Historic Linux French keyboard layout (fr-latin9)
// Copyright (c) 199x, 2002 Rene Cougnenc (original work)
//                          Guylhem Aznar <clavier @ externe.net> (maintainer)
//                          Nicolas Mailhot <Nicolas.Mailhot @ laposte.net>
//                              (XFree86 submission)
//
//  $XFree86: xc/programs/xkbcomp/symbols/fr-latin9,v 1.1 2002/12/20 02:07:11 dawes Exp $
//
// This layout has long been distributed and refined outside official channels.
// To this day it remains more feature-rich and popular than the 'fr' layout.
//
// This file is derived from an original version by Guylhem Aznar.  The
// original version is always available from:
// http://en.tldp.org/HOWTO/Francophones-HOWTO.html
// and is distributed under a GPL license.
//
// The author has given permission for this derived version to be distributed
// under the standard XFree86 license. He would like all changes to this
// version be send to him at <clavier @ externe.net> so he can sync the
// identically named linux console map (kbd, linux-console) and this
// fileout-of-tree GPL version.
//
// Now follows the keyboard design description in French.
// (If you can't read it you probably have no business changing this file anyway:)
//
// Les accents circonflexes des principales voyelles sont obtenus avec
// la touche Alt_Gr, les trémas sont obtenus par Alt_Gr + Shift.
//
//
//  ____                                     _________ _____________ _______
// | S A| S = Shift,  A = AltGr + Shift     | Imprime | Arrêt défil | Pause |
// | s a| s = normal, a = AltGr             |  Exec   |             | Halte |
//  ¯¯¯¯                                     ¯¯¯¯¯¯¯¯¯ ¯¯¯¯¯¯¯¯¯¯¯¯¯ ¯¯¯¯¯¯¯
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
// │ Œ ” │ 1&~ │ 2Éú │ 3"ű │ 4'´ │ 5(¨ │ 6-¦ │ 7Èù │ 8_ū │ 9Çų │ 0ÀÀ │ ° ° │ + ± ┃ ⌫ Retour┃
// │ œ “ │ & · │ é ~ │ " # │ ' { │ ( [ │ - | │ è ` │ _ \ │ ç ^ │ à @ │ ) ] │ = } ┃  arrière┃
// ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
// ┃       ┃ A Æ │ Z Ζ │ E ε │ R Я │ T θ │ Y ¥ │ U Û │ I ͺ │ O Œ │ P Π │ ¨ ˇ │ £ ∫ ┃Entrée ┃
// ┃Tab ↹  ┃ a æ │ z ζ │ e € │ r ρ │ t Θ │ y υ │ u û │ i ι │ o œ │ p π │ ^ ˘ │ $ ∂ ┃   ⏎   ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃        ┃ Q τ │ S Σ │ D Δ │ F Φ │ G Γ │ H Ð │ J ﬂ │ K Ξ │ L Λ │ M  │ % Ù │ µ ̄  ┃      ┃
// ┃Maj ⇬   ┃ q α │ s σ │ d δ │ f φ │ g γ │ h η │ j ﬁ │ k ξ │ l λ │ m μ │ ù ' │ * ` ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃       ┃ > ≥ │ W Ω │ X Ψ │ C ® │ V ← │ B ß │ N → │ ? , │ . . │ / ∕ │ § − ┃             ┃
// ┃Shift ⇧┃ < ≤ │ w ω │ x ψ │ c © │ v ⍽ │ b β │ n ¬ │ , ¿ │ ; × │ : ÷ │ ! ¡ ┃Shift ⇧      ┃
// ┣━━━━━━━╋━━━━━┷━┳━━━┷━━━┱─┴─────┴─────┴─────┴─────┴─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━┳━━━┛
// ┃       ┃       ┃       ┃ ␣         Espace fine insécable ⍽ ┃       ┃       ┃       ┃
// ┃Ctrl   ┃Meta   ┃Alt    ┃ ␣ Espace       Espace insécable ⍽ ┃AltGr ⇮┃Menu   ┃Ctrl   ┃
// ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━┛
partial alphanumeric_keys

xkb_symbols "latingrecmath" {

    include "latin"

    name[Group1]="France - Alternative";

    key <TLDE>	{ [              oe,              OE, leftdoublequotemark, rightdoublequotemark ] };
    key <AE01>	{ [       ampersand,               1,          periodcentered,       dead_tilde] };
    key <AE02>	{ [          eacute,               2,          asciitilde,               dead_acute] };
    key <AE03>	{ [        quotedbl,               3,          numbersign,              dead_doubleacute] };
    key <AE04>	{ [      apostrophe,               4,           braceleft,                acute ] };
    key <AE05>	{ [       parenleft,               5,         bracketleft,            diaeresis ] };
    key <AE06>	{ [           minus,               6,                 bar,            brokenbar ] };
    key <AE07>	{ [          egrave,               7,          dead_grave,           dead_grave ] };
    key <AE08>	{ [      underscore,               8,           backslash,               dead_macron ] };
    key <AE09>	{ [        ccedilla,               9,         dead_cedilla,             dead_ogonek ] };
    key <AE10>	{ [          agrave,               0,                  at,               Agrave ] };
    key <AE11>	{ [      parenright,          degree,        bracketright,           dead_abovering ] };
    key <AE12>	{ [           equal,            plus,          braceright,       dead_abovering ] };

    key <AD01>	{ [               a,               A,         	   ae,                AE] };
    key <AD02>	{ [               z,               Z,            Greek_zeta,	Greek_ZETA] };
    key <AD03>	{ [               e,               E,            EuroSign,                 Greek_epsilon ] };
    key <AD04>	{ [               r,               R,            Greek_rho,        Cyrillic_ya ] };
    key <AD05>	{ [               t,               T,            Greek_theta,       Greek_THETA]};
//tcedilla,             Tcedilla ] };
    key <AD06>	{ [               y,               Y,              Greek_nu,               yen] };
    key <AD07>	{ [               u,               U,         Greek_upsilon,       Greek_UPSILON] };
    key <AD08>	{ [               i,               I,         Greek_iota,          0x100037a] };
    key <AD09>	{ [               o,               O,         oe,           OE ] };
    key <AD10>	{ [               p,               P,         Greek_pi,            Greek_PI] };
    key <AD11>	{ [ dead_circumflex,  dead_diaeresis,      dead_breve, 					dead_caron ] };
    key <AD12>	{ [          dollar,	    sterling,         0x1002202,           0x100222b] };

    key <AC01>	{ [               q,               Q,         Greek_alpha,       Greek_tau] };
    key <AC02>	{ [               s,               S,         Greek_sigma,       Greek_SIGMA]};
//   scedilla,             Scedilla ] };
    key <AC03>	{ [               d,               D,         Greek_delta,           Greek_DELTA ] };
    key <AC04>	{ [               f,               F,         Greek_phi,          Greek_PHI] };
    key <AC05>	{ [               g,               G,         Greek_gamma,        Greek_GAMMA] };
    key <AC06>	{ [               h,               H,         Greek_eta,                  ETH ] };
    key <AC07>	{ [               j,               J,         0x100fb01,           0x100fb02] };
    key <AC08>	{ [               k,               K,         Greek_xi,           Greek_XI ] };
    key <AC09>	{ [               l,               L,         Greek_lambda,          Greek_LAMBDA ] };
    key <AC10>	{ [               m,               M,         Greek_mu,            Greek_chi ] };
    key <AC11>	{ [          ugrave,         percent,         twosuperior,               Ugrave ] };
    key <BKSL>  { [        asterisk,              mu,       threesuperior,                  yen ] };

    key <LSGT>	{ [            less,         greater,        lessthanequal,      greaterthanequal ] };
    key <AB01>	{ [               w,               W,       Greek_omega,	     Greek_OMEGA] };
    key <AB02>	{ [               x,               X,      Greek_psi,			Greek_PSI  ] };
    key <AB03>	{ [               c,               C,           copyright, 		registered] };
    key <AB04>	{ [               v,               V,          registered		      				] };
    key <AB05>	{ [               b,               B,          Greek_beta,                ssharp] };
    key <AB06>	{ [               n,               N,             ntilde,                Ntilde ] };
    key <AB07>	{ [           comma,        question,        questiondown,            0x1000326    ] };
    key <AB08>	{ [       semicolon,          period,            NoSymbol, 			dead_abovedot ] };
    key <AB09>	{ [           colon,           slash,            division, 			NoSymbol   ] };
    key <AB10>	{ [          exclam,         section,          exclamdown                       ] };

    key <SPCE>  { [           space,           space,        nobreakspace                       ] };

    // French uses a comma as decimal separator, but keyboards are labeled with a period
    // Will take effect when KP_Decimal is mapped to the locale decimal separator
    key <KPDL>  { [       KP_Delete,          period,           KP_Delete,           KP_Decimal ] };

    include "level3(ralt_switch)"
};


// $XKeyboardConfig: xkeyboard-config/symbols/fr,v 1.21 2008-04-22 19:54:11 svu Exp $

// based on a keyboard map from an 'xkb/symbols/fr' file
//
// $XFree86: xc/programs/xkbcomp/symbols/fr,v 1.2 2002/11/22 04:03:28 dawes Exp $

partial default alphanumeric_keys
xkb_symbols "basic" {

    include "latin"

    name[Group1]="France";

    key <AE01>	{ [ ampersand,          1,  onesuperior,   exclamdown ]	};
    key <AE02>	{ [    eacute,          2,   asciitilde,    oneeighth ]	};
    key <AE03>	{ [  quotedbl,          3,   numbersign,     sterling ]	};
    key <AE04>	{ [apostrophe,          4,    braceleft,       dollar ]	};
    key <AE05>	{ [ parenleft,          5,  bracketleft, threeeighths ]	};
    key <AE06>	{ [     minus,          6,          bar,  fiveeighths ]	};
    key <AE07>	{ [    egrave,          7,        grave, seveneighths ]	};
    key <AE08>	{ [underscore,          8,    backslash,    trademark ]	};
    key <AE09>	{ [  ccedilla,          9,  asciicircum,    plusminus ]	};
    key <AE10>	{ [    agrave,          0,           at,       degree ]	};
    key <AE11>	{ [parenright,     degree, bracketright, questiondown ]	};
    key <AE12>	{ [     equal,       plus,   braceright,  dead_ogonek ]	};

    key <AD01>	{ [         a,          A,           ae,           AE ]	};
    key <AD02>	{ [         z,          Z, guillemotleft,        less ]	};
    key <AD03>	{ [         e,          E,     EuroSign,         cent ]	};
    key <AD11>	{ [dead_circumflex, dead_diaeresis, dead_diaeresis, dead_abovering ] };
    key <AD12>	{ [    dollar,   sterling,     currency,  dead_macron ]	};

    key <AC01>	{ [         q,          Q,           at,  Greek_OMEGA ]	};
    key <AC10>	{ [         m,          M,           mu,    masculine ]	};
    key <AC11>	{ [    ugrave,    percent, dead_circumflex, dead_caron]	};
    key <TLDE>	{ [twosuperior, asciitilde,     notsign,      notsign ]	};

    key <BKSL>	{ [  asterisk,         mu,   dead_grave,   dead_breve ]	};
    key <AB01>	{ [         w,          W,      lstroke,      Lstroke ]	};
    key <AB07>	{ [     comma,   question,   dead_acute, dead_doubleacute ] };
    key <AB08>	{ [ semicolon,     period, horizconnector,   multiply ]	};
    key <AB09>	{ [     colon,      slash, periodcentered,   division ]	};
    key <AB10>	{ [    exclam,    section, dead_belowdot, dead_abovedot ] };

    include "nbsp(level4nl)"
    include "level3(ralt_switch)"
};

// Unicode French derivative
// Loose refactoring of the historic Linux French keyboard layout
//
// Copyright © 2006-2008 Nicolas Mailhot <nicolas.mailhot @ laposte.net>
//
// Credits (fr-latin1, fr-latin0, fr-latin9)
//   © 199x-1996 René Cougnenc ✝
//   © 1997-2002 Guylhem Aznar <clavier @ externe.net>
//   © 2003-2006 Nicolas Mailhot <nicolas.mailhot @ laposte.net>
//
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
// │ ³ ¸ │ 1 ̨  │ 2 É │ 3 ˘ │ 4 — │ 5 – │ 6 ‑ │ 7 È │ 8 ™ │ 9 Ç │ 0 À │ ° ≠ │ + ± ┃ ⌫ Retour┃
// │ ² ¹ │ & ˇ │ é ~ │ " # │ ' { │ ( [ │ - | │ è ` │ _ \ │ ç ^ │ à @ │ ) ] │ = } ┃  arrière┃
// ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
// ┃       ┃ A Æ │ Z Â │ E ¢ │ R Ê │ T Þ │ Y Ÿ │ U Û │ I Î │ O Œ │ P Ô │ ¨ ˚ │ £ Ø ┃Entrée ┃
// ┃Tab ↹  ┃ a æ │ z â │ e € │ r ê │ t þ │ y ÿ │ u û │ i î │ o œ │ p ô │ ^ ~ │ $ ø ┃   ⏎   ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃        ┃ Q Ä │ S „ │ D Ë │ F ‚ │ G ¥ │ H Ð │ J Ü │ K Ï │ L Ŀ │ M Ö │ % Ù │ µ ̄ ┃      ┃
// ┃Maj ⇬   ┃ q ä │ s ß │ d ë │ f ‘ │ g ’ │ h ð │ j ü │ k ï │ l ŀ │ m ö │ ù ' │ * ` ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃       ┃ > ≥ │ W “ │ X ” │ C ® │ V ← │ B ↑ │ N → │ ? … │ . . │ / ∕ │ § − ┃             ┃
// ┃Shift ⇧┃ < ≤ │ w « │ x » │ c © │ v ⍽ │ b ↓ │ n ¬ │ , ¿ │ ; × │ : ÷ │ ! ¡ ┃Shift ⇧      ┃
// ┣━━━━━━━╋━━━━━┷━┳━━━┷━━━┱─┴─────┴─────┴─────┴─────┴─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━┳━━━┛
// ┃       ┃       ┃       ┃ ␣         Espace fine insécable ⍽ ┃       ┃       ┃       ┃
// ┃Ctrl   ┃Meta   ┃Alt    ┃ ␣ Espace       Espace insécable ⍽ ┃AltGr ⇮┃Menu   ┃Ctrl   ┃
// ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━┛
partial alphanumeric_keys
xkb_symbols "oss" {

    include "latin"
    include "level3(ralt_switch)"
    include "nbsp(level4nl)"
    include "keypad(oss)"

    name[Group1]="France - Alternative";

    // First row
    key <TLDE>	{ [      twosuperior,    threesuperior,          onesuperior,          dead_cedilla ] }; // ² ³ ¹ ¸
    key <AE01>	{ [        ampersand,                1,           dead_caron,           dead_ogonek ] }; // & 1 ˇ ̨
    key <AE02>	{ [           eacute,                2,           asciitilde,                Eacute ] }; // é 2 ~ É
    key <AE03>	{ [         quotedbl,                3,           numbersign,            dead_breve ] }; // " 3 # ˘
    key <AE04>	{ [       apostrophe,                4,            braceleft,             0x1002014 ] }; // ' 4 { — (tiret cadratin)
    key <AE05>	{ [        parenleft,                5,          bracketleft,             0x1002013 ] }; // ( 5 [ – (tiret demi-cadratin)
    key <AE06>	{ [            minus,                6,                  bar,             0x1002011 ] }; // - 6 | ‑ (tiret insécable)
    key <AE07>	{ [           egrave,                7,                grave,                Egrave ] }; // è 7 ` È
    key <AE08>	{ [       underscore,                8,            backslash,             trademark ] }; // _ 8 \ ™
    key <AE09>	{ [         ccedilla,                9,          asciicircum,              Ccedilla ] }; // ç 9 ^ Ç
    key <AE10>	{ [           agrave,                0,                   at,                Agrave ] }; // à 0 @ À
    key <AE11>	{ [       parenright,           degree,         bracketright,              notequal ] }; // ) ° ] ≠
    key <AE12>	{ [            equal,             plus,           braceright,             plusminus ] }; // = + } ±

    // Second row
    key <AD01>	{ [                a,                A,                   ae,                    AE ] }; // a A æ Æ
    key <AD02>	{ [                z,                Z,          acircumflex,           Acircumflex ] }; // z Z â Â
    key <AD03>	{ [                e,                E,             EuroSign,                  cent ] }; // e E € ¢
    key <AD04>	{ [                r,                R,          ecircumflex,           Ecircumflex ] }; // r R ê Ê
    key <AD05>	{ [                t,                T,                thorn,                 THORN ] }; // t T þ Þ
    key <AD06>	{ [                y,                Y,           ydiaeresis,            Ydiaeresis ] }; // y Y ÿ Ÿ
    key <AD07>	{ [                u,                U,          ucircumflex,           Ucircumflex ] }; // u U û Û
    key <AD08>	{ [                i,                I,          icircumflex,           Icircumflex ] }; // i I î Î
    key <AD09>	{ [                o,                O,                   oe,                    OE ] }; // o O œ Œ
    key <AD10>	{ [                p,                P,          ocircumflex,           Ocircumflex ] }; // p P ô Ô
    key <AD11>	{ [  dead_circumflex,   dead_diaeresis,           dead_tilde,        dead_abovering ] }; // ^ ̈ ̃ ˚
    key <AD12>	{ [           dollar,         sterling,               oslash,              Ooblique ] }; // $ £ ø Ø

    // Third row
    key <AC01>	{ [                q,                Q,           adiaeresis,            Adiaeresis ] }; // q Q ä Ä
    key <AC02>	{ [                s,                S,               ssharp,    doublelowquotemark ] }; // s S ß „
    key <AC03>	{ [                d,                D,           ediaeresis,            Ediaeresis ] }; // d D ë Ë
    key <AC04>	{ [                f,                F,  leftsinglequotemark,    singlelowquotemark ] }; // f F ‘ ‚
    key <AC05>	{ [                g,                G, rightsinglequotemark,                   yen ] }; // g G ’ ¥
    key <AC06>	{ [                h,                H,                  eth,                   ETH ] }; // h H ð Ð
    key <AC07>	{ [                j,                J,           udiaeresis,            Udiaeresis ] }; // j J ü Ü
    key <AC08>	{ [                k,                K,           idiaeresis,            Idiaeresis ] }; // k K ï Ï
    key <AC09>	{ [                l,                L,            0x1000140,             0x100013F ] }; // l L ŀ Ŀ
    key <AC10>	{ [                m,                M,           odiaeresis,            Odiaeresis ] }; // m M ö Ö
    key <AC11>	{ [           ugrave,          percent,           dead_acute,                Ugrave ] }; // ù % ' Ù
    key <BKSL>	{ [         asterisk,               mu,           dead_grave,           dead_macron ] }; // * µ ` ̄

    // Fourth row
    key <LSGT>  { [             less,          greater,        lessthanequal,      greaterthanequal ] }; // < > ≤ ≥
    key <AB01>  { [                w,                W,        guillemotleft,   leftdoublequotemark ] }; // w W « “
    key <AB02>  { [                x,                X,       guillemotright,  rightdoublequotemark ] }; // x X » ”
    key <AB03>  { [                c,                C,            copyright,            registered ] }; // c C © ®
    key <AB04>  { [                v,                V,            0x100202F,             leftarrow ] }; // v V ⍽ ← (espace fine insécable)
    key <AB05>  { [                b,                B,            downarrow,               uparrow ] }; // b B ↓ ↑
    key <AB06>  { [                n,                N,              notsign,            rightarrow ] }; // n N ¬ →
    key <AB07>  { [            comma,         question,         questiondown,             0x1002026 ] }; // , ? ¿ …
    key <AB08>  { [        semicolon,           period,             multiply,             0x10022C5 ] }; // ; . × ⋅
    key <AB09>  { [            colon,            slash,             division,             0x1002215 ] }; // : / ÷ ∕
    key <AB10>  { [           exclam,          section,           exclamdown,             0x1002212 ] }; // ! § ¡ −
};
