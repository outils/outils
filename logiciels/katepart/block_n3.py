

# Dublin Core
dc = """abstract, accessRights, accrualMethod, accrualPeriodicity, accrualPolicy, alternative, audience, available, bibliographicCitation, conformsTo, contributor, coverage, created, creator, date, dateAccepted, dateCopyrighted, dateSubmitted, description, educationLevel, extent, format, hasFormat, hasPart, hasVersion, identifier, instructionalMethod, isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, issued, isVersionOf, language, license, mediator, medium, modified, provenance, publisher, references, relation, replaces, requires, rights, rightsHolder, source, spatial, subject, tableOfContents, temporal, title, type, valid,
contributor, coverage, creator, date, description, format, identifier, language, publisher, relation, rights, source, subject, title, type,
DCMIType, DDC, IMT, LCC, LCSH, MESH, NLM, TGN, UDC,
Box, ISO3166, ISO639-2, ISO639-3, Period, Point, RFC1766, RFC3066, RFC4646, RFC5646, URI, W3CDTF,
Agent, AgentClass, BibliographicResource, FileFormat, Frequency, Jurisdiction, LicenseDocument, LinguisticSystem, Location, LocationPeriodOrJurisdiction, MediaType, MediaTypeOrExtent, MethodOfAccrual, MethodOfInstruction, PeriodOfTime, PhysicalMedium, PhysicalResource, Policy, ProvenanceStatement, RightsStatement, SizeOrDuration, Standard,
Collection, Dataset, Event, Image, InteractiveResource, MovingImage, PhysicalObject, Service, Software, Sound, StillImage, Text,
memberOf, VocabularyEncodingScheme"""
dc_elems = set([])
for subject in dc.split(',') :
  dc_elems.add(subject.strip())
#for subject in sorted(dc_elems) :
  #print '      <item>dc:%s</item>' % subject


from rdflib import Graph

# rdf with definition online (n3 or rdf/xml files)

rdfs = [
  #('rdfs','http://www.w3.org/2000/01/rdf-schema#', None),
  #('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',None),
  #('owl', 'http://www.w3.org/2002/07/owl#', 'n3'),
  ('foaf', 'http://xmlns.com/foaf/0.1/', None),
  ('doap', 'http://usefulinc.com/ns/doap#', None),
  ]

for prefix, rdf, fmt in rdfs :
  subjects = set([])
  g = Graph()
  g.parse(rdf, format=fmt)
  for subject in g.subjects() :
    subjects.add(subject)
    
  for subject in sorted(subjects) :
    if str(rdf) == str(subject) : continue
    term = subject.replace(rdf, '')
    if term.startswith('http') : # ignore other namespaces
      continue
    print '      <item>%s:%s</item>' % (prefix, term)



  